<?php
set_include_path(dirname(__FILE__));
require_once('lib/methods/HTTPRequest_Type.abstract.php');
require_once('lib/methods/HTTPRequest_COOKIE.class.php');

class CookieStub extends HTTPRequest_Cookie {

    public function inject($key,$val) {
        $this->data[$key] = $val;
    }

    public function hydrateCookie($data) {
        if (!$data)
            return null;

        foreach ($data as $K => $V) {
            if ( is_array($V)) {
                $this->set($K,serialize($V));
            } else {
                if ($K != "TOKEN")
                    $this->set($K,$V);
            }
        }
    }
}

class CookieTest extends PHPUnit_Framework_TestCase {

    public $validData = array(
            'string' => 'pete',
            'bool' => true,
            'int' => '666',
            'float' => '1.510'
        );

    public $badData = array(
            'bool' => 'fadlse',
            'int' => 'my name is pete',
            'float' => 'no float'
        );

    public $commonData = array(
            'array' => array(1,2,3,4,5),
            'email' => 'pete@freakzero.com',
            'userID' => 1,
            'array' => 'a:4:{i:0;i:1;i:1;s:4:"zwei";i:2;i:3;i:3;s:4:"vier";}'
            );

    public function getInstance($data) {
        return new CookieStub($data,array('xss' => 2,'salt' => 'myawesomesalt'));
    }

    public function testDynamicSetGet() {
        $z = $this->getInstance($this->commonData);
        $z->iAm = 'BATMAN';
        $this->assertEquals('BATMAN',$z->iAm);
    }

    public function testGetGood() {
        $z = $this->getInstance($this->validData);

        foreach ($this->validData as $key => $v) {
            $expect = $z->get($key);
            $this->assertInternalType($key,$expect);
        }
    }

    public function testGetBad() {
        $z = $this->getInstance($this->badData);
        foreach ($this->badData as $key => $v) {
            $expect = $z->get($key,$key);
            $this->assertNull($expect);
        }
    }

    public function testGetArray() {
        $z = $this->getInstance($this->commonData);

        $array = $z->getArray('array');
        $this->assertNotEmpty($array);
    }

    public function testSetSaveGet() {
        $z = $this->getInstance($this->commonData);

        $value = 'SPAAAAAAAAAARTAAAAAAAA';
        $z->set('thisIs',$value)->save();

        $this->assertEquals($value,$z->get('thisIs'));
    }

    public function testValidateGet() {
        $z = $this->getInstance($this->commonData);
        $this->assertEquals('pete@freakzero.com',$z->getValidated('email', 'email'));
    }

    public function testValidateGetFail() {
        $z = $this->getInstance($this->commonData);
        $this->assertNull($z->getValidated('email', 'domain'));
    }

    public function testRemove() {
        $z = $this->getInstance($this->commonData);
        $z->remove('array');
        $this->assertEmpty($z->getArray('array'));

    }
    public function testInject() {
        $z = $this->getInstance($this->commonData);

        $this->assertEquals(1,$z->get('userID'));

        // inject... No Token Update = Bad
        $z->inject('userID',777);

        $this->assertNull($z->get('userID'));
    }

    public function testArrayInject() {
        $z = $this->getInstance($this->commonData);

        // inject... No Token Update = Bad
        $z->inject('userID',777);

        $this->assertNull($z->getArray('array'));
    }
}