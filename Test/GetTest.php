<?php
set_include_path(dirname(__FILE__));
require_once('lib/methods/HTTPRequest_Type.abstract.php');
require_once('lib/methods/HTTPRequest_GET.class.php');

class GetTest extends PHPUnit_Framework_TestCase {

    public $validData = array(
            'string' => 'pete',
            'bool' => true,
            'int' => '666',
            'float' => '1.510'
        );

    public $badData = array(
            'bool' => 'fadlse',
            'int' => 'my name is pete',
            'float' => 'no float'
        );

    public $commonData = array(
                'array' => array(1,2,3,4,5),
                'email' => 'pete@freakzero.com'
            );

    public function getInstance($data) {
        return new HTTPRequest_Get($data,array('xss' => 2));
    }

    public function testDynamicSetGet() {
        $z = $this->getInstance($this->commonData);
        $z->iAm = 'BATMAN';
        $this->assertEquals('BATMAN',$z->iAm);
    }

    public function testGetGood() {
        $z = $this->getInstance($this->validData);

        foreach ($this->validData as $key => $v) {
            $expect = $z->get($key);
            $this->assertInternalType($key,$expect);
        }
    }

    public function testGetBad() {
        $z = $this->getInstance($this->badData);
        foreach ($this->badData as $key => $v) {
            $expect = $z->get($key,$key);
            $this->assertNull($expect);
        }
    }

    public function testSet() {
        $z = $this->getInstance($this->badData);

        $z->set('integerofthebeast',666);
        $this->assertEquals(666,$z->get('integerofthebeast'));
    }

    public function testValidateGet() {
        $z = $this->getInstance($this->commonData);
        $this->assertEquals('pete@freakzero.com',$z->getValidated('email', 'email'));
    }

    public function testValidateGetFail() {
        $z = $this->getInstance($this->commonData);
        $this->assertNull($z->getValidated('email', 'domain'));
    }





}