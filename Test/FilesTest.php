<?php
set_include_path(dirname(__FILE__));
require_once('lib/methods/HTTPRequest_Type.abstract.php');
require_once('lib/methods/HTTPRequest_FILES.class.php');

class HTTPRequest_FilesStub extends HTTPRequest_Files {
    protected function uploadFileWrapper($file,$destination){
        return copy($file,$destination);
    }
}

class FilesTest extends PHPUnit_Framework_TestCase {

    public $fileData = array(
        "textFile" => array(
            "name" => "textfile.txt",
            "type" => "text/plain",
            "tmp_name" => "Test/files/textfile.txt",
            "error" => 0,
            "size" => 215
        ),

        "validFile" => array(
            "name" => "realimage.jpg",
            "type" => "image/jpeg",
            "tmp_name" => "Test/files/realimage.jpg",
            "error" => 0,
            "size" => 215
        ),

        "fakeImage" => array(
            "name" => "fake.txt",
            "type" => "text/plain",
            "tmp_name" => "Test/files/fake.txt",
            "error" => 0,
            "size" => 215
        )
    );

    public function getInstance($data) {
        return new HTTPRequest_FilesStub($data,array('xss' => 2,'overwrite' => false));
    }

    public function tearDown() {
        @unlink('Test/files/saved.jpg');
    }

    /**
    * @expectedException RuntimeException
    */
    public function testReadOnlyException() {
        $z = $this->getInstance($this->fileData);
        $z->iAm = 'BATMAN';
    }

    public function testGetName() {
        $z = $this->getInstance($this->fileData);
        $expect = $z->selectFile('validFile')->getName();
        $this->assertEquals($this->fileData['validFile']['name'],$expect);
    }

    public function testGetMime() {
        $z = $this->getInstance($this->fileData);

        $jpg = $z->selectFile('validFile')->getMime();
        $this->assertEquals($this->fileData['validFile']['type'],$jpg);

        $txt = $z->selectFile('textFile')->getMime();
        $this->assertEquals($this->fileData['textFile']['type'],$txt);

        $no = $z->selectFile('nofilehere')->getMime();
        $this->assertNull($no);

    }

    public function testCatchFakeMime() {
        $z = $this->getInstance($this->fileData);
        $fake = $z->selectFile('fakeImage')->getMime();
        $this->assertEquals('image/png',$fake);
    }

    public function testGetSize() {
        $z = $this->getInstance($this->fileData);
        $actual = $z->selectFile('validFile')->getSize();
        $this->assertInternalType('integer',$actual);
    }

    public function testSaveFile() {
        $root = dirname(__FILE__);
        $z = $this->getInstance($this->fileData);
        $uploaded = $z->selectFile('validFile')->saveFile('saved.jpg','Test/files/');
        $this->assertTrue($uploaded);
    }

}