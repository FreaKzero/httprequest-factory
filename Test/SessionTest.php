<?php
set_include_path(dirname(__FILE__));
require_once('lib/methods/HTTPRequest_Type.abstract.php');
require_once('lib/methods/HTTPRequest_SESSION.class.php');

class SessionInjectable extends HTTPRequest_Session {

    public function inject($key,$val) {
        $this->data[$key] = $val;
    }
}

class SessionTest extends PHPUnit_Framework_TestCase {

    public $validData = array(
            'string' => 'pete',
            'bool' => true,
            'int' => '666',
            'float' => '1.510'
        );

    public $badData = array(
            'bool' => 'fadlse',
            'int' => 'my name is pete',
            'float' => 'no float'
        );

    public $commonData = array(
                'array' => array(1,2,3,4,5),
                'email' => 'pete@freakzero.com',
                'userID' => 1
            );

    public function getInstance($data) {
        return new HTTPRequest_Session($data,array('xss' => 2,'salt' => 'myawesomesalt','handle-session' => false));
    }

    public function testDynamicSetGet() {
        $z = $this->getInstance($this->commonData);
        $z->iAm = 'BATMAN';
        $this->assertEquals('BATMAN',$z->iAm);
    }

    public function getInjectable($data) {
        return new SessionInjectable($data,array('xss' => 2,'salt' => 'myawesomesalt','handle-session' => false));
    }
    public function testGetGood() {
        $z = $this->getInstance($this->validData);

        foreach ($this->validData as $key => $v) {
            $expect = $z->get($key);
            $this->assertInternalType($key,$expect);
        }
    }

    public function testGetBad() {
        $z = $this->getInstance($this->badData);
        foreach ($this->badData as $key => $v) {
            $expect = $z->get($key,$key);
            $this->assertNull($expect);
        }
    }

    public function testGetArray() {
        $z = $this->getInstance($this->commonData);

        $array = $z->getArray('array');
        $this->assertNotEmpty($array);
    }

    public function testSetSaveGet() {
        $z = $this->getInstance($this->commonData);

        $value = 'SPAAAAAAAAAARTAAAAAAAA';
        $z->set('thisIs',$value)->save();

        $this->assertEquals($value,$z->get('thisIs'));
    }

    public function testValidateGet() {
        $z = $this->getInstance($this->commonData);
        $this->assertEquals('pete@freakzero.com',$z->getValidated('email', 'email'));
    }

    public function testValidateGetFail() {
        $z = $this->getInstance($this->commonData);
        $this->assertNull($z->getValidated('email', 'domain'));
    }

    public function testRemove() {
        $z = $this->getInstance($this->commonData);
        $z->remove('array');
        $this->assertEmpty($z->getArray('array'));

    }
    public function testInject() {
        $z = $this->getInjectable($this->commonData);

        $this->assertEquals(1,$z->get('userID'));

        // inject... No Token Update = Bad
        $z->inject('userID',777);

        $this->assertNull($z->get('userID'));
    }

    public function testArrayInject() {
        $z = $this->getInjectable($this->commonData);

        // inject... No Token Update = Bad
        $z->inject('userID',777);

        $this->assertNull($z->getArray('array'));
    }
}