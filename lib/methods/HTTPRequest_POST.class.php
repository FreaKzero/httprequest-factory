<?php
/**
 * @uses RequestType
 * @version 0.2
 * @package HTTPRequest
 * @author freakzero <pete@freakzero.com>
 */
class HTTPRequest_Post extends RequestType {

    /**
     * Sanitize Data
     *
     * @package HTTPRequest
     * @access public
     */
    public function __construct($data,$config) {
        $this->write = true;
        $this->data = $this->buildRequestData($data,$config['xss']);
    }

    /**
     * Save Data
     *
     * @package HTTPRequest
     * @access public
     */
    public function __destruct() {
        $this->save();
    }

    /**
     * Returns Array from $this->data
     *
     * @package HTTPRequest
     * @param string $key
     * @return array
     * @access public
     */
    public function getArray($key){
        return $this->arrayAccess($key);
    }

    /**
     * Hydrate Session from $this->data
     *
     * @package HTTPRequest
     * @access public
     */
    public function save() {
        $_POST = $this->data;
    }

    /**
     * Get validated String via RequestType::$validate
     *
     * @package HTTPRequest
     * @param string $key   key of Element in Data where $value is stored
     * @return  mixed       if validated return Data otherwise null
     * @access public
     */
    public function getValidated($key,$validate){
        return $this->validateAccess($key, $validate);
    }

    /**
     * Sets Data
     *
     * @package HTTPRequest
     * @param string $key   key of Element in Data where $value is stored
     * @param mixed $value  Data you want to store
     * @return object $this
     * @access public
     */
    public function set($key,$value){
        $this->$key = $value;
        return $this;
    }

   /**
     * Gets Data
     *
     * @package HTTPRequest
     * @param string $key
     * @param string $type  default set on String
     * @return mixed $this->$key
     * @access public
     */
    public function get($key,$type=null){
        if ($this->checkType($this->$key,$type))
            return $this->$key;

        return null;
    }
}

?>