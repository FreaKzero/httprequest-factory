<?php
/**
 * @uses RequestType
 * @version 0.2
 * @package HTTPRequest
 * @author freakzero <pete@freakzero.com>
 */
class HTTPRequest_Get extends RequestType {

    /**
     * sanitize Data
     *
     * @package HTTPRequest
     * @access public
     */
    public function __construct($data,$config) {
        $this->write = true;
        $this->data = $this->buildRequestData($data,$config['xss']);
    }

    /**
     * Get validated String via RequestType::$validate
     *
     * @package HTTPRequest
     * @param string $key   key of Element in Data where $value is stored
     * @return  mixed       if validated return Data otherwise null
     * @access public
     */
    public function getValidated($key,$validate){
        return $this->validateAccess($key, $validate);
    }

    /**
     * Sets Data
     *
     * @package HTTPRequest
     * @param string $key   key of Element in Data where $value is stored
     * @param mixed $value  Data you want to store
     * @return object $this
     * @access public
     */
    public function set($key,$value){
        if (is_array($value))
            return null;

        $this->$key = $value;
        return $this;
    }

   /**
     * Gets Data
     *
     * @package HTTPRequest
     * @param string $key
     * @param string $type  default set on String
     * @return mixed $this->$key
     * @access public
     */
    public function get($key,$type=null){
        if ($this->checkType($this->$key,$type))
            return $this->$key;

        return null;
    }

    /**
     * Build a Querystring from $data and Redirect via HTTP 302
     *
     * @package HTTPRequest
     * @param string $uri
     * @param integer $HTTPSTATUS
     * @access public
     */
    public function redirect($uri,$HTTPSTATUS=302){
        header('Location: '.$uri.'?'.http_build_query($this->data));
        exit();
    }
}

?>