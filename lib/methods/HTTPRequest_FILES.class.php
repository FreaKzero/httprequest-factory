<?php
/**
 * @uses RequestType
 * @version 0.2
 * @package HTTPRequest
 * @author freakzero <pete@freakzero.com>
 */
class HTTPRequest_Files extends RequestType {

    private $cursor = null;

    /**
     * Unserialize Cookie Arrays, sanitize Data and set Token
     *
     * @package HTTPRequest
     * @access public
     */
    public function __construct($data,$config) {
        self::$config = $config;
        $this->write = false;
        $this->data = $data;
    }

    /**
     * Selects File from Data
     *
     * @package HTTPRequest
     * @param string $key
     * @return object $this
     * @access public
     */
    public function selectFile($key) {
        if (isset($this->data[$key])) {
            $this->cursor = $key;
        } else {
            $this->cursor = null;
        }

        return $this;
    }

   /**
     * Saves selected File to a destination
     *
     * @package HTTPRequest
     * @param string $destination
     * @param string $filename
     * @return boolean  true on success
     * @access public
     */
    public function saveFile($filename=false,$destination='') {
         if (!$this->cursor)
            return null;

         if (!$filename)
            $filename = $this->getName();

        $file = $destination.$filename;

        if ( !self::$config['overwrite'] && file_exists($file) )
            return false;

        return $this->uploadFileWrapper($this->data[$this->cursor]['tmp_name'],$file);
    }

    /**
     * Gets Name of File
     *
     * @package HTTPrequest
     * @return string name of File
     * @access public
     */
    public function getName(){
        if (!$this->cursor)
            return null;

        return $this->data[$this->cursor]['name'];
    }

  /**
     * Gets Size of File
     *
     * @package HTTPrequest
     * @return integer Bytesize of File
     * @access public
     */
    public function getSize(){
        if (!$this->cursor)
            return null;

        return $this->data[$this->cursor]['size'];
    }

    /**
     * Gets MIME Type of File
     *
     * @package HTTPRequest
     * @return string MIMEType of File
     * @access public
     */
    public function getMime(){
        if (!$this->cursor)
            return null;

        if ($img = getimagesize($this->data[$this->cursor]['tmp_name'])) {
            return $img['mime'];
        } elseif (isset($this->data[$this->cursor]['type'])) {
            return $this->data[$this->cursor]['type'];
        }

        return false;
    }

    /**
     * Upload File Wrapper
     *
     * @package HTTPRequest
     * @param string $file
     * @param string $destination
     * @return boolean
     * @access private
     */
    protected function uploadFileWrapper($file,$destination){
        if (is_uploaded_file($file))
            return move_uploaded_file($file,$destination);

        return false;
    }
}

?>