<?php
/**
 * @uses RequestType
 * @version 0.2
 * @package HTTPRequest
 * @author freakzero <pete@freakzero.com>
 */
class HTTPRequest_Cookie extends RequestType {

    /**
     * Unserialize Cookie Arrays, sanitize Data and set Token
     *
     * @package HTTPRequest
     * @access public
     */
    public function __construct($data,$config) {
        $this->write = true;
        $this->useToken = true;
        self::$config = $config;

        foreach($data as $k => $v) {
            // hiding Notice - but no other way :'(
            $array = @unserialize($v);
            if ($array && is_array($array)) {
                $this->data[$k] = $array;
            } else {
                $this->data[$k] = $v;
            }
        }

        $this->data = $this->buildRequestData($this->data,$config['xss']);
        $this->setToken();
    }

    /**
     * Regenerate Session ID and saves the Data
     *
     * @package HTTPRequest
     * @access public
     */
    public function __destruct() {
        $this->save();
    }

    /**
     * Kills Cookie
     *
     * @package HTTPRequest
     * @access public
     */
    public function kill(){
        $this->data = null;
        return null;
    }

    /**
     * Returns Array from $this->data
     *
     * @package HTTPRequest
     * @param string $key
     * @return array
     * @access public
     */
    public function getArray($key){
        return $this->arrayAccess($key);
    }

    /**
     * @package HTTPRequest
     * @param string $key   key of Element in Data where $value is stored
     * @return object $this
     * @access public
     */
    public function remove($key){
        $this->removeAccess($key);
        return $this;
    }

    /**
     * Save data as Cookie
     *
     * @package HTTPRequest
     * @access public
     */
    public function save(){
        $this->hydrateCookie($this->data);
    }

    /**
     * Cookie Save Wrapper
     *
     * @package HTTPRequest
     * @param array $data
     * @access public
     */
    public function hydrateCookie($data) {
        $expire = time()+3600*self::$conf['expire'];

        foreach ($data as $K => $V) {
            if ( is_array($V)) {
                setcookie($K,serialize($V),
                    $expire,
                    self::$config['path'],
                    self::$config['domain'],
                    self::$config['secure'],
                    self::$config['httponly']
                );
            } else {
                setcookie($K,$V,
                    $expire,
                    self::$config['path'],
                    self::$config['domain'],
                    self::$config['secure'],
                    self::$config['httponly']
                );
            }
        }
    }

    /**
     * Get validated String via RequestType::$validate
     *
     * @package HTTPRequest
     * @param string $key   key of Element in Data where $value is stored
     * @return  mixed       if validated return Data otherwise null
     * @access public
     */
    public function getValidated($key,$validate){
        return $this->validateAccess($key, $validate);
    }

    /**
     * Sets Data
     *
     * @package HTTPRequest
     * @param string $key   key of Element in Data where $value is stored
     * @param mixed $value  Data you want to store
     * @return object $this
     * @access public
     */
    public function set($key,$value){
        $this->$key = $value;
        return $this;
    }

   /**
     * Gets Data
     *
     * @package HTTPRequest
     * @param string $key
     * @param string $type  default set on String
     * @return mixed $this->$key
     * @access public
     */
    public function get($key,$type=null){
        if ($this->checkType($this->$key,$type))
            return $this->$key;

        return null;
    }
}

?>