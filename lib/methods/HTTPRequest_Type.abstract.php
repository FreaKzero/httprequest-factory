<?php
/**
 * Request Types Abstract Class
 *
 * @version 0.2
 * @package HTTPRequest
 * @author freakzero <pete@freakzero.com>
 */
abstract class RequestType {

    protected $data = null;
    protected $write = true;
    protected $useToken = false;

    static $config = null;
    static $notOverwrite = array(
        'TOKEN',
        'PHPSESSID'
    );

    static $validate = array(
        'email' => '^[a-zA-Z0-9+&*-]+(?:\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\.)+[a-zA-Z]{2,7}$^',
        'domain' => '=^(http|https|ftp):\/\/(?:[a-zA-ZüöäÜÖÄ0-9][\wüöäÜÖÄ.-]*\.)*[a-zA-ZüöäÜÖÄ0-9][\wüöäÜÖÄ._-]*\.[a-zA-Z]{2,}$=',
        'ip' => '=^((\d{1,2}|1\d{2}|2[0-4]\d|25[0-5])\.){3}(\d{1,2}|1\d{2}|2[0-4]\d|25[0-5])$=',
        'date' => '%^(0?[1-9]|[12][0-9]|3[01])[- /.](0?[1-9]|1[012])[- /.](19|20)?[\d]{2}$%',
        'alnum' => '/^[a-zA-Z0-9äöüßAÖÜ]+$/'
    );

    abstract function __construct($DATA,$CONFIG);

    public function __get($key) {
        if (!is_array($this->data) || !isset($this->data[$key]))
            return null;

        if ($this->useToken)
            if (!$this->checkToken())
                return $this->kill();

        return $this->data[$key];
    }

    public function __set($key, $value) {
        if ($this->write === false)
            throw new RuntimeException(sprintf('%s instance is Read-Only', get_class($this)));

        if (in_array($key,self::$notOverwrite))
            throw new RuntimeException(sprintf($key. ' cannot be Overwritten', get_class($this)));

        $this->data[$key] = $value;

        if ($this->useToken)
            $this->setToken();

        return $this;
    }

    /**
     * Checks if Request is from an Ajax Request
     *
     * @package HTTPRequest
     * @return boolean
     * @access public
     */
    public function isAjax(){
        if (!$this->xhr)
            return false;

        return true;
    }

    /**
     * Unsets Data from $this->data
     *
     * @package HTTPRequest
     * @param string $key
     * @access protected
     */
    protected function removeAccess($key){
        unset($this->data[$key]);

        if ($this->useToken)
            $this->setToken();
    }

    /**
     * Get Array from Data
     *
     * @package HTTPRequest
     * @param string $key
     * @return array
     * @access protected
     */
    protected function arrayAccess($key){
        if (is_array($this->data) && isset($this->data[$key]))
            return $this->$key;

        return array();
    }

    /**
     * Get pre-validated Data - Helpermethod
     *
     * @package HTTPRequest
     * @param string $key   key from $this->daata
     * @param string $vkey  key from self::$validate
     * @return mixed validated Value from $this->data
     * @access protected
     */
    protected function validateAccess($key, $vkey){
        if ( key_exists($vkey, self::$validate) && preg_match(self::$validate[$vkey],$this->data[$key]) )
            return $this->$key;

        return null;

    }

    /**
     * Checks if $data is $type
     *
     * @package HTTPRequest
     * @param mixed $data  data to check
     * @param string $type  datatype to check
     * @return boolean
     * @access protected
     */
    protected function checkType($data,$type){
        switch (strtolower($type)) {
            case null:
                return true;
            break;

            case 'int':
                if (!is_int($data))
                    return false;
            break;

            case 'bool':
                if (!is_bool($data))
                    return false;
            break;

            case 'float':
                if (!is_float($data))
                    return false;
            break;

            case 'numeric':
                if (!is_numeric($data))
                    return false;
            break;

            case 'array':
                if (!is_array($data))
                    return false;
            break;

            case 'string':
                if (!is_string($data))
                return false;
            break;

            default:
                trigger_error('Datatype: '.$type.' is not supported');
            break;
        }

        return true;
    }

    /**
     * Set Data to null
     *
     * @package HTTPRequest
     * @access protected
     */
    protected function kill(){
      $this->data = null;
      return null;
    }

    /**
     * Generates a Token with data serializing and a given salt from Config
     * saves it in the first level of Session or Cookie
     *
     * @package HTTPRequest
     * @access protected
     */
    protected function setToken() {
        if ( isset($this->data['TOKEN']))
            unset($this->data['TOKEN']);

        $this->data['TOKEN'] = crc32(serialize($this->data).self::$config['salt']);
    }

    /**
     * checks the inserted Tokenhash with actual Data in Session or Cookie
     *
     * @package HTTPRequest
     * @return boolean      true on success false on fail
     * @access protected
     */
    protected function checkToken() {
        if ( !isset($this->data['TOKEN']) )
            return false;

        $proof = $this->data['TOKEN'];
        unset($this->data['TOKEN']);
        if ( $proof != crc32(serialize($this->data).self::$config['salt']) ) {
            return false;
        } else {
            $this->data['TOKEN'] = crc32(serialize($this->data).self::$config['salt']);
            return 1;
        }
    }

    /**
     * Build Request Data with new casted Datatypes
     *
     * @package HTTPRequest
     * @return boolean      true on success false on fail
     * @access protected
     */
    protected function buildRequestData($array,$XSS=1){
        foreach ($array as $k => $v) {
            if (is_numeric($v)) {
                $v = $v + 0;
                if ( is_int($v) ) {
                    $array[$k] = (int) $v;
                } elseif ( is_float($v) ) {
                    $array[$k] = (float) $v;
                }
            } elseif (is_bool($v)) {
                $array[$k] = (bool) $v;
            } elseif (is_array($v)) {
                $array[$k] = $this->buildRequestData($array[$k]);
            } else {
                if ($XSS > 0) {
                    switch ($XSS) {
                        case 1:
                            $array[$k] = htmlentities(trim($v));
                        break;

                        case 2:
                            $array[$k] = strip_tags(trim($v));
                        break;
                    }
                } else {
                    $array[$k] = (string) trim($v);
                }
            }
        }
        return $array;
    }
}
?>