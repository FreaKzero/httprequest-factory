<?php
/**
 * @uses RequestType
 * @version 0.2
 * @package HTTPRequest
 * @author freakzero <pete@freakzero.com>
 */
class HTTPRequest_Session extends RequestType {

    /**
     * Session is writeable, sanitizes Data and starts the session
     *
     * @package HTTPRequest
     * @access public
     */
    public function __construct($data,$config) {
        if ($config['handle-session'])
            session_start();

        $this->write = true;
        $this->useToken = true;
        self::$config = $config;
        $this->data = $this->buildRequestData($data,$config['xss']);
        $this->setToken();
    }

    /**
     * Regenerate Session ID and saves the Data
     *
     * @package HTTPRequest
     * @access public
     */
    public function __destruct() {
        if (self::$config['handle-session'])
            session_regenerate_id();

        $this->save();
    }

    /**
     * Returns Array from $this->data
     *
     * @package HTTPRequest
     * @param string $key
     * @return array
     * @access public
     */
    public function getArray($key){
        return $this->arrayAccess($key);
    }

    /**
     * @package HTTPRequest
     * @param string $key   key of Element in Data where $value is stored
     * @return object $this
     * @access public
     */
    public function remove($key){
        $this->removeAccess($key);
        return $this;
    }

    /**
     * Hydrate Session from $this->data
     *
     * @package HTTPRequest
     * @access public
     */
    public function save(){
        $_SESSION = $this->data;
    }

    /**
     * Get validated String via RequestType::$validate
     *
     * @package HTTPRequest
     * @param string $key   key of Element in Data where $value is stored
     * @return  mixed       if validated return Data otherwise null
     * @access public
     */
    public function getValidated($key,$validate){
        return $this->validateAccess($key, $validate);
    }

    /**
     * Sets Data
     *
     * @package HTTPRequest
     * @param string $key   key of Element in Data where $value is stored
     * @param mixed $value  Data you want to store
     * @return object $this
     * @access public
     */
    public function set($key,$value){
        $this->$key = $value;
        return $this;
    }

   /**
     * Gets Data
     *
     * @package HTTPRequest
     * @param string $key
     * @param string $type  default set on String
     * @return mixed $this->$key
     * @access public
     */
    public function get($key,$type=null){
        if ($this->checkType($this->$key,$type))
        return $this->$key;

        return null;
    }
}

?>