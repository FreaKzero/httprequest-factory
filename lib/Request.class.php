<?php
/**
 * HTTPRequest Factory
 *
 * @example HTTPRequest::method('METHOD')
 * @version 0.2
 * @package HTTPRequest
 * @author freakzero <pete@freakzero.com>
 */
class HTTPRequest {

    const LAZYLOAD_DIR = 'methods/';

    protected static $session = null;
    protected static $post = null;
    protected static $cookie = null;
    protected static $get = null;
    protected static $files = null;

    protected static $config = array(
        'COOKIE' => array(
            'xss' => 2,
            'expire'      => 24,
            'path'        => '/',
            'domain'      => '',
            'secure'      => false,
            'httponly'    => false,
            'salt' => 'h324923gbf&%/§$8ihf3$§$)U")F§J"asdf'
        ),
        'SESSION' => array(
            'xss' => 2,
            'handle-session' => false,
            'salt' => 'h324923gbf&%/§$8ihf3$§$)U")F§J"asdf'
        ),
        'POST' => array(
            'xss' => 2,
        ),
        'GET' => array(
            'xss' => 2
        ),
        'FILES' => array(
            'overwrite' => true
        )
    );

    /**
     * Loads the abstract Class and dependent Files for Class instantiation
     *
     * @package HTTPRequest
     * @param string $method
     * @access private
     */
    private static function _fileLoader($type) {
        if (!require_once(self::LAZYLOAD_DIR.'HTTPRequest_Type.abstract.php'))
            throw new RuntimeException(sprintf('Cant load abstract RequestType for Factory HTTPRequest', get_class()));

        if (!require_once(self::LAZYLOAD_DIR.'HTTPRequest_'.$type.'.class.php'))
            throw new RuntimeException(sprintf('Cant load "'.$type.'" for Factory HTTPRequest', get_class()));
    }
    /**
     * Config Setter
     *
     * @package HTTPRequest
     * @param string $type
     * @param array $config
     * @access public
     */
    public static function config($type,$config) {
        foreach (self::$config[$type] AS $K => $V)
            if (isset(self::$config[$type][$K]))
                self::$config[$type][$K] = $V;
    }

    /**
     * Factory Initiator
     * Loads the driver and returns the Instance of HTTPRequest Method
     *
     * @package HTTPRequest
     * @param string $type  Methodtype
     * @return object self::$instance   Instance of Method
     * @access public
     */
    public static function method($type) {
        $type = strtoupper($type);

        if ( isset(self::$config[$type]) ) {
            self::_fileLoader($type);

            $method = 'get'.$type;
            return self::$method();
        } else {
            throw new RuntimeException(sprintf('HTTPRequest for "'.$type.'" is not supported', get_class()));
        }
    }

    /**
     * Returns Session Object
     *
     * @package HTTPRequest
     * @return object HTTPRequest_Session
     * @access public
     */
    public function getSESSION(){
        self::$session = new HTTPRequest_Session($_SESSION,self::$config['SESSION']);
        return self::$session;
    }

    /**
     * Returns HTTPRequest_Cookie Object
     *
     * @package HTTPRequest
     * @return object HTTPRequest_Cookie
     * @access public
     */
    public function getCOOKIE(){
        self::$cookie = new HTTPRequest_Cookie($_COOKIE,self::$config['COOKIE']);
        return self::$cookie;
    }

    /**
     * Returns HTTPRequest_Post Object
     *
     * @package HTTPRequest
     * @return object HTTPRequest_Post
     * @access public
     */
    public function getPOST(){
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            self::$instance->xhr = true;

        self::$post = new HTTPRequest_Post($_POST,self::$config['POST']);
        return self::$post;
    }

    /**
     * Returns HTTPRequest_Get Object
     *
     * @package HTTPRequest
     * @return object HTTPRequest_Get
     * @access public
     */
    public function getGET(){
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
            self::$instance->xhr = true;

        self::$get = new HTTPRequest_Get($_GET,self::$config['GET']);
        return self::$get;
    }

    /**
     * Returns HTTPRequest_Files Object
     *
     * @package HTTPRequest
     * @return object HTTPRequest_Files
     * @access public
     */
    public function getFILES(){
        self::$files = new HTTPRequest_Files($_FILES,self::$config['FILES']);
        return new HTTPRequest_Files($_FILES);
    }
}

?>